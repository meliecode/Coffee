import React, { useContext, useEffect, useState } from 'react'
import Image from 'next/image'
import styles from './steps.module.scss'

import { Layout, Dots, Step1, Step2, Step3, Step4 } from '@components'
import { UserContext } from '@context'

const Steps = () => {
	const { activeStep, setActiveStep, infos } = useContext(UserContext)

	useEffect(() => {
		return () => {
			setActiveStep(1)
		}
	}, [setActiveStep])

	useEffect(() => {
		console.log('infos', infos)
	}, [infos])

	return (
		<Layout>
			<div className={styles.container}>
				<Image className={styles.image} src="/images/plante.svg" width={130} height={210} alt="Plante" />
				{activeStep === 1 && <Step1 />}
				{activeStep === 2 && <Step2 />}
				{activeStep === 3 && <Step3 />}
				{activeStep === 4 && <Step4 />}
				<Dots step={activeStep} />
			</div>
		</Layout>
	)
}

export default Steps
