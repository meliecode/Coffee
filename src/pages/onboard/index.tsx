import React from 'react'
import Image from 'next/image'
import styles from './index.module.scss'
import texts from '../../styles/texts.module.scss'
import { useRouter } from 'next/router'

import { Layout, Title, Button } from '@components'

const Starter = () => {
	const router = useRouter()

	const handleClick = () => {
		window.localStorage.setItem('steps', '1')
		void router.push(`/onboard/steps`)
	}

	return (
		<Layout>
			<div className={styles.container}>
				<Image src="/images/onboard.svg" width={300} height={300} alt="Onboard" />

				<div className={styles.infos}>
					<Title>
						Trouves l&#39;endroit <span>idéal</span> pour travailler, étudier et réfléchir
					</Title>
					<Button onClick={handleClick}>
						<div className={texts.primaryButton}>Commencer</div>
					</Button>
				</div>
			</div>
		</Layout>
	)
}

export default Starter
