import React from 'react'
import Image from 'next/image'
import type { NextPage, GetStaticPaths, GetStaticProps } from 'next'
import Head from 'next/head'
import { useRouter } from 'next/router'

import { coffees, items, items as itemsList } from '@resources'
import { Favoris, Layout, Previous, Buttons, Icon, MiniCard, Gauge } from '@components'
import styles from './[id].module.scss'

const Coffee: NextPage = ({ coffee }: any) => {
	const router = useRouter()

	return (
		<Layout>
			<Head>
				<title>Coffee</title>
			</Head>
			<div className={styles.container}>
				<Previous onClick={() => router.push(`/result`)} />
				<div className={styles.header}>
					<h1>{coffee.name}</h1>
					<Favoris />
				</div>
				<div className={styles.inner}>
					<div className={styles.intro}>
						<Image
							className={styles.image}
							src={`/images/coffees/${coffee.img[0]}.png`}
							layout="fill"
							alt={coffee.name}
						/>
						<div>
							<span className={styles.label}>Adresse : </span>
							<span className={styles.under}>{coffee.address}</span>
						</div>
						<div className={styles.schedules}>
							<span className={styles.label}>Horaires :</span>
							<span className={styles.open}> Ouvert </span>
							<span>Lundi 9h - 18h </span>
							<Icon name="down" />
						</div>
						<div>
							<span className={styles.label}>Sur place</span>
							<div>
								{itemsList.map(elem => {
									if (coffee.items.includes(elem.name)) {
										return (
											<MiniCard key={elem.id}>
												<Icon name={elem.img} />
												<span>{elem.name}</span>
											</MiniCard>
										)
									}
								})}
							</div>
						</div>
					</div>
					<div className={styles.more}>
						<div className={styles.top}>
							<div
								dangerouslySetInnerHTML={{
									__html: coffee.desc,
								}}
							/>
							<div className={styles.affluence}>
								<span className={styles.label}>Affluence en temps réel</span>
								<div>
									<Gauge number={9} active={true} percent={25} />
									<Gauge number={10} active={true} percent={33} />
									<Gauge number={11} active={true} percent={30} />
									<Gauge number={12} active={true} percent={60} />
									<Gauge number={13} active={true} percent={65} />
									<Gauge number={14} active={true} percent={50} />
									<Gauge number={15} active={false} percent={45} />
									<Gauge number={16} active={false} percent={40} />
									<Gauge number={17} active={false} percent={80} />
									<Gauge number={18} active={false} percent={85} />
									<Gauge number={19} active={false} percent={80} />
									<Gauge number={20} active={false} percent={70} />
								</div>
							</div>
						</div>
						<div className={styles.bottom}>
							<span className={styles.label}>Galerie d&#39;images</span>
							<div>
								{coffee.img.map((img: string) => (
									<Image
										key={img}
										className={styles.image}
										src={`/images/coffees/${img}.png`}
										layout="fill"
										alt={coffee.name}
									/>
								))}
							</div>
						</div>
					</div>
				</div>
			</div>
			<Buttons />
		</Layout>
	)
}

export const getStaticPaths: GetStaticPaths = async () => {
	const paths = coffees.map(coffee => {
		return {
			params: {
				id: coffee.id.toString(),
			},
		}
	})

	return {
		paths,
		fallback: false,
	}
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
	const filtered = coffees.filter(coffee => coffee.id.toString() === params?.id)

	return {
		props: {
			coffee: filtered[0],
		},
	}
}

export default Coffee
