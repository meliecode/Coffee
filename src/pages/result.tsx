import React from 'react'
import styles from './result.module.scss'
import texts from '../styles/texts.module.scss'
import { useRouter } from 'next/router'

import { coffees } from '../resources/coffees'

import { Layout, Title, CoffeeCard, Buttons } from '@components'

const Result = () => {
	return (
		<Layout>
			<div className={styles.container}>
				<Title>Voici ton top 5 :</Title>
				<div className={styles.inner}>
					{coffees.map((item, i) => (
						<CoffeeCard key={i} item={item} i={i} />
					))}
				</div>
				<Buttons />
			</div>
		</Layout>
	)
}

export default Result
