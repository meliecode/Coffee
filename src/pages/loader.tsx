import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from './loader.module.scss'
import { useRouter } from 'next/router'

import { Layout, Title, Button } from '@components'

const Loader = () => {
	const router = useRouter()

	useEffect(() => {
		const timer = setTimeout(() => {
			void router.push(`/result`)
		}, 8000)
		return () => clearTimeout(timer)
	}, [router])

	return (
		<Layout>
			<div className={styles.container}>
				<div className={styles.inner}>
					<Title>
						Nous recherchons <span>l&#39;endroit idéal</span> ...
					</Title>

					<Image src="/images/loader.svg" width={283} height={252} alt="Loader" />
				</div>
				{/* <div className={styles.ldsEllipsis}>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div> */}

				<div className={styles.loader}>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>
			</div>
		</Layout>
	)
}

export default Loader
