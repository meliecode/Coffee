import React, { useEffect } from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'
import { useRouter } from 'next/router'

import { Layout } from '@components'

const Index: NextPage = () => {
	const router = useRouter()

	useEffect(() => {
		void router.push(`/onboard`)
	}, [router])

	return (
		<Layout>
			<Head>
				<title>Mes cafés</title>
				<meta name="description" content="Mes cafés" />
				<link rel="icon" href="/favicon.ico" />
			</Head>
		</Layout>
	)
}

export default Index
