import React, { useState } from 'react'
import { createContext, Dispatch, SetStateAction } from 'react'

export const UserContext = createContext<{
	activeStep: number
	setActiveStep: Dispatch<SetStateAction<number>>
	infos: {
		mode: string
		tools: string
		items: SetStateAction<Set<unknown>>
		date: SetStateAction<Date | undefined>
		time: string
	}
	setInfos: Dispatch<SetStateAction<any>>
}>({
	activeStep: 1,
	setActiveStep: () => null,
	infos: {
		mode: '',
		tools: '',
		items: new Set(),
		date: new Date(),
		time: '',
	},
	setInfos: () => null,
})

interface PropTypes {
	children: React.ReactNode
}

export const UserProvider: React.FC<PropTypes> = ({ children }) => {
	const [activeStep, setActiveStep] = useState(1)
	const [infos, setInfos] = useState({
		mode: '',
		tools: '',
		items: new Set(),
		date: new Date(),
		time: '',
	})

	return <UserContext.Provider value={{ activeStep, setActiveStep, infos, setInfos }}>{children}</UserContext.Provider>
}
