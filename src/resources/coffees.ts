export interface CoffeeTypes {
	id: number | string
	img: string[]
	name: string
	items: string[]
	address: string
	schedules: string
	desc: string
}

export const coffees: CoffeeTypes[] = [
	{
		id: 0,
		img: ['0-0-kaldi'],
		name: 'Café Kaldi',
		items: ['wifi', 'prises', 'musique'],
		address: '12-18 Udagawacho, Shibuya City, Tokyo 150-0042, Japon',
		schedules: '',
		desc: 'Hands Cafe is cozy with bright, natural light and is nestled in the corner of the physics and chemistry supplies floor. Think Nature meets Science and Technology. It has a lovely wood interior with communal or private seating, terrace seating, and a long table against the windows with four Macbooks for use for free. Behind the computers sits a lovely outdoor cactus and plant garden that patrons can browse freely.',
	},
	{
		id: 1,
		img: ['1-0-cozy'],
		name: 'Café Cozy',
		items: ['wifi', 'prises', 'musique'],
		address: '12-18 Udagawacho, Shibuya City, Tokyo 150-0042, Japon',
		schedules: '',
		desc: 'Hands Cafe is cozy with bright, natural light and is nestled in the corner of the physics and chemistry supplies floor. Think Nature meets Science and Technology. It has a lovely wood interior with communal or private seating, terrace seating, and a long table against the windows with four Macbooks for use for free. Behind the computers sits a lovely outdoor cactus and plant garden that patrons can browse freely.',
	},
	{
		id: 2,
		img: ['2-0-tokyu', '2-1-tokyu', '2-2-tokyu', '4-0-starbs'],
		name: 'Tokyu Hands',
		items: ['wifi', 'prises', 'musique'],
		address: '12-18 Udagawacho, Shibuya City, Tokyo 150-0042, Japon',
		schedules: '',
		desc: 'Hands Cafe is cozy with bright, natural light and is nestled in the corner of the physics and chemistry supplies floor. Think Nature meets Science and Technology. It has a lovely wood interior with communal or private seating, terrace seating, and a long table against the windows with four Macbooks for use for free. Behind the computers sits a lovely outdoor cactus and plant garden that patrons can browse freely.',
	},
	{
		id: 3,
		img: ['3-0-pam'],
		name: 'Prêt-à-manger - Le Peletier',
		items: ['wifi', 'prises', 'confort', 'café spé'],
		address: '12-18 Udagawacho, Shibuya City, Tokyo 150-0042, Japon',
		schedules: '',
		desc: 'Hands Cafe is cozy with bright, natural light and is nestled in the corner of the physics and chemistry supplies floor. Think Nature meets Science and Technology. It has a lovely wood interior with communal or private seating, terrace seating, and a long table against the windows with four Macbooks for use for free. Behind the computers sits a lovely outdoor cactus and plant garden that patrons can browse freely.',
	},
	{
		id: 4,
		img: ['4-0-starbs'],
		name: 'Starbucks Omotesando',
		items: ['wifi', 'prises', 'confort', 'café spé'],
		address: '12-18 Udagawacho, Shibuya City, Tokyo 150-0042, Japon',
		schedules: '',
		desc: 'Hands Cafe is cozy with bright, natural light and is nestled in the corner of the physics and chemistry supplies floor. Think Nature meets Science and Technology. It has a lovely wood interior with communal or private seating, terrace seating, and a long table against the windows with four Macbooks for use for free. Behind the computers sits a lovely outdoor cactus and plant garden that patrons can browse freely.',
	},
]
