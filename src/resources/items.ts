export interface Types {
	id: number
	img: string
	name: string
}

export const items: Types[] = [
	{
		id: 0,
		img: '0-spe-coffee',
		name: 'café spé.',
	},
	{
		id: 1,
		img: '1-hot-coffee',
		name: 'boisson chaude',
	},
	{
		id: 2,
		img: '2-bubble-tea',
		name: 'bubble tea',
	},
	{
		id: 3,
		img: '3-cake',
		name: 'gâteau',
	},
	{
		id: 4,
		img: '4-wifi',
		name: 'wifi',
	},
	{
		id: 5,
		img: '5-prise',
		name: 'prises',
	},
	{
		id: 6,
		img: '6-location',
		name: 'proche',
	},
	{
		id: 7,
		img: '7-outside',
		name: 'extérieur',
	},
	{
		id: 8,
		img: '8-session',
		name: 'longue session',
	},
	{
		id: 9,
		img: '9-music',
		name: 'musique',
	},
	{
		id: 10,
		img: '10-focus',
		name: 'focus',
	},
	{
		id: 11,
		img: '11-confort',
		name: 'confort',
	},
	{
		id: 12,
		img: '12-luminosity',
		name: 'luminosité',
	},
	{
		id: 13,
		img: '13-calm',
		name: 'calme',
	},
	{
		id: 14,
		img: '14-vegan',
		name: 'vegan',
	},
	{
		id: 15,
		img: '15-food',
		name: 'restaurant',
	},
	{
		id: 16,
		img: '16-originality',
		name: 'originalité',
	},
	{
		id: 17,
		img: '17-spacious',
		name: 'spacieux',
	},
	{
		id: 18,
		img: '18-ice-cream',
		name: 'glaces',
	},
	{
		id: 19,
		img: '19-cold-drink',
		name: 'boisson fraîche',
	},
]
