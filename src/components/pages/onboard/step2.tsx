import React, { useContext } from 'react'
import Image from 'next/image'
import styles from './step2.module.scss'
import step from './step.module.scss'

import { Title, Card, Location, Previous } from '@components'
import { UserContext } from '@context'

const Step2 = () => {
	const { activeStep, setActiveStep, infos, setInfos } = useContext(UserContext)

	const handleClick = (number: number) => {
		if (number === 1) {
			setInfos({ ...infos, tools: 'books' })
			window.localStorage.setItem('tools', 'books')
		} else if (number === 2) {
			setInfos({ ...infos, tools: 'desktop' })
			window.localStorage.setItem('tools', 'desktop')
		}
		window.localStorage.setItem('steps', '3')
		setActiveStep(3)
	}

	return (
		<div className={step.container}>
			<div className={step.header}>
				<Previous onClick={() => setActiveStep(activeStep - 1)} />
				<Title bold={true}>Tu es plutôt...</Title>
				<div className={step.location}></div>
			</div>
			<div className={styles.inner}>
				<Card active={infos.tools === 'books' ? true : false} onClick={() => handleClick(1)}>
					<span>Livre, stylo et cahier</span>
					<Image src="/images/shelf.svg" width={93} height={58} alt="Shelf" />
				</Card>
				<Card active={infos.tools === 'desktop' ? true : false} onClick={() => handleClick(2)}>
					<span>Ordinateur et tablette</span>
					<Image src="/images/screen.svg" width={96} height={62} alt="Screen" />
				</Card>
			</div>
			<Location />
		</div>
	)
}

export default Step2
