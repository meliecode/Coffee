import React, { useContext, useState, useEffect } from 'react'
import Image from 'next/image'
import step from './step.module.scss'
import styles from './step3.module.scss'
import texts from '../../../styles/texts.module.scss'

import { Previous, Title, MiniCard, Icon, Button } from '@components'
import { UserContext } from '@context'
import { items as itemsList } from '@resources'

const Step3 = () => {
	const { activeStep, setActiveStep, infos, setInfos } = useContext(UserContext)
	const [items, setItems] = useState(new Set())

	useEffect(() => {
		if (infos.items) setItems(infos.items)
	}, [infos])

	const handleClick = () => {
		setInfos({ ...infos, items: items })
		window.localStorage.setItem('items', JSON.stringify(Array.from(items)))

		window.localStorage.setItem('steps', '4')
		setActiveStep(4)
	}

	const handleSelectionItems = (item: { id: number }) => {
		const newSet = new Set(items)

		if (items.size > 0 && items.has(item.id)) {
			newSet.delete(item.id)
			setItems(newSet)
		} else if (items.size < 5) {
			newSet.add(item.id)
			setItems(newSet)
		}
	}

	return (
		<div className={step.container}>
			<div className={step.header}>
				<Previous onClick={() => setActiveStep(activeStep - 1)} />
				<Title bold={true}>Sélectionne 5 items</Title>
				<div className={step.location}></div>
			</div>
			<div className={styles.inner}>
				{itemsList &&
					itemsList.map((item: { id: number; img: string; name: string }, i: number) => (
						<MiniCard
							key={i}
							active={items.size > 0 && items.has(item.id) ? true : false}
							onClick={() => handleSelectionItems(item)}
						>
							<Icon name={item.img} active={items.size > 0 && items.has(item.id) ? true : false} />
							<span>{item.name}</span>
						</MiniCard>
					))}
			</div>
			<div className={styles.selectedInfos}>
				Tu as sélectionné <span>{items.size > 0 ? items.size : '0'}</span> items sur <span>5</span>.
			</div>
			<Button onClick={handleClick}>
				<div className={texts.secondaryButton}>Étape suivante</div>
			</Button>
		</div>
	)
}

export default Step3
