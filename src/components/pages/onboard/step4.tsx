import React, { useContext, useState, useEffect } from 'react'
import { DayPicker } from 'react-day-picker'
import 'react-day-picker/dist/style.css'
import fr from 'date-fns/locale/fr'
import { useRouter } from 'next/router'

import step from './step.module.scss'
import styles from './step4.module.scss'
import texts from '../../../styles/texts.module.scss'

import { Title, Button, Previous } from '@components'
import { UserContext } from '@context'

const Step4 = () => {
	const router = useRouter()
	const { infos, setInfos, activeStep, setActiveStep } = useContext(UserContext)
	const [selected, setSelected] = useState<Date>()
	const [time, setTime] = useState('10:00')

	useEffect(() => {
		console.log('time', time)
	}, [time])

	useEffect(() => {
		if (infos.date) setSelected(infos.date)
	}, [infos])

	const handleClick = () => {
		setInfos({ ...infos, date: selected, time: time })
		if (selected) window.localStorage.setItem('date', selected.toString())
		if (time) window.localStorage.setItem('time', time)
		window.localStorage.setItem('steps', 'loader')
		void router.push(`/loader`)
	}

	return (
		<div className={step.container}>
			<div className={step.header}>
				<Previous onClick={() => setActiveStep(activeStep - 1)} />
				<Title bold={true}>Quand souhaites-tu t&#39;y rendre ?</Title>
				<div className={step.location}></div>
			</div>
			<div className={styles.inner}>
				<DayPicker
					className={styles.date}
					mode="single"
					selected={selected}
					onSelect={setSelected}
					modifiersClassNames={{
						selected: styles.selected,
						today: styles.today,
					}}
					// showOutsideDays
					locale={fr}
					modifiers={{
						disabled: [
							{
								before: new Date(),
							},
						],
					}}
				/>
				<div className={styles.time}>
					<div>Heure du rendez-vous :</div>
					<div className={styles.picker}>
						<input
							type="time"
							id="appt"
							name="appt"
							min="09:00"
							max="18:00"
							value={time}
							onChange={e => setTime(e.target.value)}
						></input>
					</div>
				</div>
			</div>
			<Button onClick={handleClick}>
				<div className={texts.secondaryButton}>Montrez-moi mon prochain spot !</div>
			</Button>
		</div>
	)
}

export default Step4
