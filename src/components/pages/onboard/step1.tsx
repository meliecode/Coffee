import React, { useContext } from 'react'
import Image from 'next/image'
import styles from './step1.module.scss'
import step from './step.module.scss'

import { Title, Card, Location } from '@components'
import { UserContext } from '@context'

const Step1 = () => {
	const { setActiveStep, infos, setInfos } = useContext(UserContext)

	const handleClick = (number: number) => {
		if (number === 1) {
			setInfos({ ...infos, mode: 'solo' })
			window.localStorage.setItem('mode', 'solo')
		} else if (number === 2) {
			setInfos({ ...infos, mode: 'several' })
			window.localStorage.setItem('mode', 'several')
		}
		window.localStorage.setItem('steps', '2')
		setActiveStep(2)
	}

	return (
		<div className={step.container}>
			<Title bold={true}>Qu&#39;as-tu de prévu aujourd&#39;hui ?</Title>
			<div className={styles.inner}>
				<Card active={infos.mode === 'solo' ? true : false} onClick={() => handleClick(1)}>
					<span>Session solo</span>
					<Image src="/images/cloud.svg" width={137} height={57} alt="Cloud" />
				</Card>
				<Card active={infos.mode === 'several' ? true : false} onClick={() => handleClick(2)}>
					<span>À plusieurs</span>
					<div className={styles.images}>
						<Image src="/images/cloud.svg" width={137} height={57} alt="Cloud" />
						<Image src="/images/cloud-small.svg" width={83} height={39} alt="Cloud small" />
					</div>
				</Card>
			</div>
			<Location />
		</div>
	)
}

export default Step1
