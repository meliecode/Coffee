import React, { useContext, useEffect, useState } from 'react'
import styles from './layout.module.scss'
import { useRouter } from 'next/router'

import { UserContext } from '@context'

interface PropsTypes {
	children: JSX.Element | JSX.Element[]
}

const Layout: React.FC<PropsTypes> = ({ children }) => {
	const router = useRouter()
	const { setActiveStep, infos, setInfos } = useContext(UserContext)

	// useEffect(() => {
	// 	console.log('infos2', infos)
	// }, [infos])

	useEffect(() => {
		if (typeof window !== undefined && window.localStorage.steps) {
			// if (window.localStorage.steps === '2') {
			// 	setActiveStep(2)
			// } else if (window.localStorage.steps === '3') {
			// 	setActiveStep(3)
			// } else if (window.localStorage.steps === '4') {
			// 	setActiveStep(4)
			// }

			if (!infos.mode && window.localStorage.mode) setInfos({ ...infos, mode: window.localStorage.mode })
			if (!infos.tools && window.localStorage.tools) setInfos({ ...infos, tools: window.localStorage.tools })
			if (!infos.date && window.localStorage.date) setInfos({ ...infos, date: window.localStorage.date })
			if (!infos.time && window.localStorage.time) setInfos({ ...infos, time: window.localStorage.time })
			if (!infos.items && window.localStorage.items) {
				const parsed = new Set(JSON.parse(window.localStorage.items))
				setInfos({ ...infos, items: parsed })
			}

			// if (window.localStorage.steps === 'loader') {
			// 	void router.push(`/loader`)
			// } else {
			// 	void router.push(`/onboard/steps`)
			// }
		} else {
			// void router.push(`/onboard`)
		}
	}, [])

	return <div className={styles.container}>{children}</div>
}

export default Layout
