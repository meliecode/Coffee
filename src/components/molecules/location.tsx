import React from 'react'
import Image from 'next/image'
import styles from './location.module.scss'

import { Button } from '@components'

const Location = () => {
	return (
		<div className={styles.container}>
			<span>You are located in Paris. To change city click here :</span>
			<button className={styles.button}>
				<Image src="/images/location.svg" width={11} height={14} alt="Location" />
				<span>Change city</span>
			</button>
		</div>
	)
}

export default Location
