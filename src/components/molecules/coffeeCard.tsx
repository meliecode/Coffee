import React from 'react'
import Image from 'next/image'
import styles from './coffeeCard.module.scss'
import Link from 'next/link'

import { Favoris } from '@components'

interface PropTypes {
	i: number
	item: {
		id: number | string
		img: string[]
		name: string
		items: string[]
		address: string
		schedules: string
		desc: string
	}
}

const CoffeeCard: React.FC<PropTypes> = ({ i, item }) => {
	console.log('item', item)
	return (
		<div className={styles.container}>
			<Link href={`/coffee/${item.id}`}>
				<h2>
					<span>#{i + 1}</span> {item.name}
				</h2>
				<Image className={styles.image} src={`/images/coffees/${item.img[0]}.png`} layout="fill" alt={item.name} />
				<ul className={styles.list}>
					<li>
						<Image src="/images/location.svg" width={7} height={9} alt="Location" /> à 2 km
					</li>
					{item && item.items ? item.items.map((elem, index) => <li key={index}>{elem}</li>) : null}
				</ul>
				<Favoris />
			</Link>
		</div>
	)
}

export default CoffeeCard
