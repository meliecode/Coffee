import React from 'react'
import styles from './gauge.module.scss'

interface PropTypes {
	number: number
	active: boolean
	percent: number
}

const Gauge: React.FC<PropTypes> = ({ number, active, percent }) => {
	return (
		<div className={styles.container}>
			<div className={styles.graph}>
				<div className={styles.full}></div>
				<div
					className={styles.percent}
					style={{
						height: `${percent}px`,
						border: !active ? '1px dashed #B5838D' : '',
						background: !active ? 'none' : '#B5838D',
					}}
				></div>
			</div>
			<div className={styles.text}>{number}</div>
		</div>
	)
}

export default Gauge
