import React from 'react'
import styles from './buttons.module.scss'
import texts from '../../styles/texts.module.scss'
import { useRouter } from 'next/router'

import { Button } from '@components'

const Buttons = () => {
	const router = useRouter()

	const newClick = () => {
		window.localStorage.removeItem('steps')
		window.localStorage.removeItem('time')
		window.localStorage.removeItem('date')
		window.localStorage.removeItem('mode')
		window.localStorage.removeItem('tools')
		window.localStorage.removeItem('items')
		void router.push(`/`)
	}

	return (
		<div className={styles.buttons}>
			<Button className={styles.primary} onClick={() => router.push(`/result`)}>
				<div className={texts.secondaryButton}>Voir plus de résultats</div>
			</Button>
			<Button className={styles.secondary} onClick={newClick}>
				<div className={texts.reverseButton}>Nouvelle recherche</div>
			</Button>
		</div>
	)
}

export default Buttons
