// PAGES // ONBOARD
import CoffeeCard from './molecules/coffeeCard'
export { default as Step1 } from './pages/onboard/step1'
export { default as Step2 } from './pages/onboard/step2'
export { default as Step3 } from './pages/onboard/step3'
export { default as Step4 } from './pages/onboard/step4'

// ORGANISMS
export { default as Layout } from './organisms/layout'

// MOLECULES
export { default as Location } from './molecules/location'
export { default as CoffeeCard } from './molecules/coffeeCard'
export { default as Buttons } from './molecules/buttons'
export { default as Gauge } from './molecules/gauge'

// ATOMS
export { default as Icon } from './atoms/icons/icon'
export { default as Title } from './atoms/title'
export { default as Button } from './atoms/button'
export { default as Card } from './atoms/card'
export { default as Dots } from './atoms/dots'
export { default as Previous } from './atoms/previous'
export { default as MiniCard } from './atoms/miniCard'
export { default as Favoris } from './atoms/favoris'
