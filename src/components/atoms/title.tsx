import React from 'react'
import styles from './title.module.scss'

interface PropTypes {
	children: React.ReactNode
	bold?: boolean
}

const Title: React.FC<PropTypes & React.ButtonHTMLAttributes<any>> = ({ children, bold }) => {
	return (
		<h1 className={styles.container} style={{ fontWeight: bold ? '800' : '700' }}>
			{children}
		</h1>
	)
}

export default Title
