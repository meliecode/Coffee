import React from 'react'

import {
	IconSpeCoffee,
	IconHotCoffee,
	IconBubbleTea,
	IconCake,
	IconWifi,
	IconPrise,
	IconLocation,
	IconOutside,
	IconSession,
	IconMusic,
	IconFocus,
	IconConfort,
	IconLuminosity,
	IconCalm,
	IconVegan,
	IconFood,
	IconOriginality,
	IconSpacious,
	IconIceCream,
	IconColdDrink,
	IconDown,
} from '@icons'

interface Props {
	name?: string | null
	active?: boolean
}

const Icon: React.FC<Props> = ({ name, active }) => {
	switch (name) {
		case '0-spe-coffee':
			return <IconSpeCoffee active={active} />
		case '1-hot-coffee':
			return <IconHotCoffee active={active} />
		case '2-bubble-tea':
			return <IconBubbleTea active={active} />
		case '3-cake':
			return <IconCake active={active} />
		case '4-wifi':
			return <IconWifi active={active} />
		case '5-prise':
			return <IconPrise active={active} />
		case '6-location':
			return <IconLocation active={active} />
		case '7-outside':
			return <IconOutside active={active} />
		case '8-session':
			return <IconSession active={active} />
		case '9-music':
			return <IconMusic active={active} />
		case '10-focus':
			return <IconFocus active={active} />
		case '11-confort':
			return <IconConfort active={active} />
		case '12-luminosity':
			return <IconLuminosity active={active} />
		case '13-calm':
			return <IconCalm active={active} />
		case '14-vegan':
			return <IconVegan active={active} />
		case '15-food':
			return <IconFood active={active} />
		case '16-originality':
			return <IconOriginality active={active} />
		case '17-spacious':
			return <IconSpacious active={active} />
		case '18-ice-cream':
			return <IconIceCream active={active} />
		case '19-cold-drink':
			return <IconColdDrink active={active} />
		case 'down':
			return <IconDown />
		default:
			return <IconSpeCoffee />
	}
}

export default Icon
