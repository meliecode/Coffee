import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconCalm: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="none" viewBox="0 0 25 25">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				d="M14.384 21.106c-.28 0-.552-.09-.777-.257l-.036-.028L8.57 16.72H4.76a1.313 1.313 0 01-1.312-1.313V9.283A1.312 1.312 0 014.759 7.97h3.81L13.57 3.87l.036-.028a1.312 1.312 0 012.09 1.057v14.894a1.312 1.312 0 01-1.312 1.313zm4.813-4.386a.876.876 0 01-.782-1.267c.52-1.034.782-2.079.782-3.108 0-1.059-.255-2.074-.779-3.102a.875.875 0 011.559-.795c.653 1.28.97 2.556.97 3.897 0 1.303-.328 2.613-.968 3.893a.875.875 0 01-.782.482z"
			></path>
		</svg>
	)
}

export default IconCalm
