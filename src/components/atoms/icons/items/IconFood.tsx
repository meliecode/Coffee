import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconFood: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="none" viewBox="0 0 25 25">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				d="M16.66 6.585v6c0 1.1.9 2 2 2h1v7c0 .55.45 1 1 1s1-.45 1-1V3.715c0-.65-.61-1.13-1.24-.98-2.16.53-3.76 2.36-3.76 3.85zm-5 3h-2v-6c0-.55-.45-1-1-1s-1 .45-1 1v6h-2v-6c0-.55-.45-1-1-1s-1 .45-1 1v6c0 2.21 1.79 4 4 4v8c0 .55.45 1 1 1s1-.45 1-1v-8c2.21 0 4-1.79 4-4v-6c0-.55-.45-1-1-1s-1 .45-1 1v6z"
			></path>
		</svg>
	)
}

export default IconFood
