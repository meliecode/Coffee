import React from 'react'

const IconDown = () => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="10" height="6" fill="none" viewBox="0 0 10 6">
			<path
				fill="#B5838D"
				d="M9.847.726L5.508 5.757a.33.33 0 01-.489 0L.681.727C.52.538.665.263.925.263h8.677c.26 0 .406.275.245.462z"
			></path>
		</svg>
	)
}

export default IconDown
