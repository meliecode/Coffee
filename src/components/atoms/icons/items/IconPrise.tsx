import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconPrise: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="none" viewBox="0 0 25 25">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				d="M8.575 9.311h8.514a2.128 2.128 0 012.128 2.129v3.724a5.855 5.855 0 01-4.421 5.677 1.597 1.597 0 01-1.432 2.305H12.3a1.596 1.596 0 01-1.431-2.305 5.855 5.855 0 01-4.422-5.677V11.44A2.128 2.128 0 018.575 9.31zm2.129-1.064H8.575V2.926a1.064 1.064 0 012.129 0v5.321zm6.385 0H14.96V2.926a1.065 1.065 0 012.129 0v5.321z"
			></path>
		</svg>
	)
}

export default IconPrise
