import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconCake: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="25" height="24" fill="none" viewBox="0 0 25 24">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				d="M12.66 1.5a2.5 2.5 0 110 5 2.5 2.5 0 010-5zM16.53 5c2.13 0 4.13 2 4.13 4 2.7 0 2.7 4 0 4h-16c-2.7 0-2.7-4 0-4 0-2 2-4 4.13-4 .44 1.73 2.01 3 3.87 3 1.86 0 3.43-1.27 3.87-3zM5.66 15h3l1 7h-2l-2-7zm5 0h4l-1 7h-2l-1-7zm6 0h3l-2 7h-2l1-7z"
			></path>
		</svg>
	)
}

export default IconCake
