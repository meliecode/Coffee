import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconOutside: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="none" viewBox="0 0 25 25">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				d="M12.66 6.862a1 1 0 001-1v-2a1 1 0 00-2 0v2a1 1 0 001 1zm9 5h-2a1 1 0 000 2h2a1 1 0 000-2zm-15 1a1 1 0 00-1-1h-2a1 1 0 000 2h2a1 1 0 001-1zm.22-7a1.012 1.012 0 00-1.39 1.47l1.44 1.39a1 1 0 00.73.28 1 1 0 00.72-.31 1 1 0 000-1.41l-1.5-1.42zm10.78 3.14a1 1 0 00.69-.28l1.44-1.39a1 1 0 00-1.35-1.47L17 7.282a1 1 0 000 1.41 1 1 0 00.66.31zm-5 9.86a1 1 0 00-1 1v2a1 1 0 102 0v-2a1 1 0 00-1-1zm5.73-1.86a1 1 0 00-1.39 1.44l1.44 1.42a1 1 0 001.41-.02 1.002 1.002 0 000-1.42l-1.46-1.42zm-11.46 0l-1.44 1.39a1 1 0 000 1.42 1 1 0 00.72.3 1 1 0 00.67-.25l1.44-1.39a1 1 0 00-1.39-1.44v-.03zm5.73-8.14a4 4 0 100 8 4 4 0 000-8z"
			></path>
		</svg>
	)
}

export default IconOutside
