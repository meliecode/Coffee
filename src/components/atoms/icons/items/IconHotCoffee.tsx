import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconHotCoffee: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="25" height="24" fill="none" viewBox="0 0 25 24">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				d="M5.447 2h2v3h-2V2zm4 0h2v3h-2V2zm4 0h2v3h-2V2zm6 7h-2V8a1 1 0 00-1-1h-12a1 1 0 00-1 1v10a3 3 0 003 3h8a3 3 0 003-3h2c1.103 0 2-.897 2-2v-5c0-1.103-.897-2-2-2zm-2 7v-5h2l.002 5h-2.002z"
			></path>
		</svg>
	)
}

export default IconHotCoffee
