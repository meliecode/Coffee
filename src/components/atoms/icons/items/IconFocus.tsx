import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconFocus: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="none" viewBox="0 0 25 25">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				d="M4.053 19.723a1.5 1.5 0 110 3 1.5 1.5 0 010-3zm5-3a2.5 2.5 0 110 5.001 2.5 2.5 0 010-5zm6-1c-1.19 0-2.27-.5-3-1.35-.73.85-1.81 1.35-3 1.35-1.96 0-3.59-1.41-3.93-3.26a4.02 4.02 0 01-2.57-3.74 4 4 0 014-4c.26 0 .5.03.77.07.73-.66 1.68-1.07 2.73-1.07 1.19 0 2.27.5 3 1.35.73-.85 1.81-1.35 3-1.35 1.96 0 3.59 1.41 3.93 3.26a4.02 4.02 0 012.57 3.74 4 4 0 01-4 4l-.77-.07c-.73.66-1.68 1.07-2.73 1.07z"
			></path>
		</svg>
	)
}

export default IconFocus
