import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconConfort: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="none" viewBox="0 0 25 25">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				d="M13.16 6.723c0-1.11.89-2 2-2h3.5c1.1 0 2 .9 2 2v2.16c-1.16.41-2 1.51-2 2.81v2.03h-5.5v-7zm-6.5 4.96v2.04h5.5v-7c0-1.11-.89-2-2-2h-3.5c-1.1 0-2 .9-2 2v2.15c1.16.41 2 1.52 2 2.81zm14.66-1.93c-.98.16-1.66 1.09-1.66 2.09v2.88h-14v-3a2 2 0 00-4 0v5c0 1.1.9 2 2 2v2h2v-2h14v2h2v-2c1.1 0 2-.9 2-2v-5c0-1.21-1.09-2.18-2.34-1.97z"
			></path>
		</svg>
	)
}

export default IconConfort
