import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconSpeCoffee: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="25" height="24" fill="none" viewBox="0 0 25 24">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				d="M6.34 18a6.06 6.06 0 005.17-6 7.62 7.62 0 016.52-7.51l2.59-.37c-.07-.08-.13-.16-.21-.24-3.26-3.26-9.52-2.28-14 2.18C2.62 9.9 1.34 15 3.1 18.46L6.34 18z"
			></path>
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				d="M13.07 12a7.63 7.63 0 01-6.51 7.52l-2.46.35.15.17c3.26 3.26 9.52 2.29 14-2.17 3.77-3.76 5.09-8.87 3.34-12.28l-3.34.48A6.05 6.05 0 0013.07 12z"
			></path>
		</svg>
	)
}

export default IconSpeCoffee
