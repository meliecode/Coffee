import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconSpacious: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="none" viewBox="0 0 25 25">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				fillRule="evenodd"
				d="M3.447 2.155a.708.708 0 01.707-.708H21.14a.708.708 0 110 1.415h-6.37v5.662a1.416 1.416 0 01-1.415 1.415H11.94a1.415 1.415 0 01-1.415-1.415V2.862h-6.37a.708.708 0 01-.707-.707zm8.492 12.03a1.415 1.415 0 00-1.415 1.416v5.661h-6.37a.708.708 0 000 1.416H21.14a.707.707 0 100-1.416h-6.37v-5.661a1.415 1.415 0 00-1.415-1.416H11.94z"
				clipRule="evenodd"
			></path>
		</svg>
	)
}

export default IconSpacious
