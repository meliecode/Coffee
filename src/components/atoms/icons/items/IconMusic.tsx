import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconMusic: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="15" height="20" fill="none" viewBox="0 0 15 20">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				fillRule="evenodd"
				d="M7.022 2.011a1.223 1.223 0 00-.353.845v11.44c-.65-.335-1.458-.534-2.333-.534-2.147 0-3.89 1.194-3.89 2.667 0 1.474 1.743 2.667 3.89 2.667 2.146 0 3.889-1.194 3.889-2.667V5.496l6.222-1.067V2.056c0-.197-.051-.392-.15-.57a1.386 1.386 0 00-.42-.46 1.667 1.667 0 00-.609-.263 1.809 1.809 0 00-.682-.014l-4.666.8a1.62 1.62 0 00-.898.462z"
				clipRule="evenodd"
			></path>
		</svg>
	)
}

export default IconMusic
