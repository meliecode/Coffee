import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconSession: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="none" viewBox="0 0 25 25">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				d="M8 2.413c-1.1 0-2 .9-2 2l.01 3.18c0 .53.21 1.03.58 1.41l3.41 3.41-3.41 3.43c-.37.37-.58.88-.58 1.41L6 20.413c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2v-3.16c0-.53-.21-1.04-.58-1.41L14 12.413l3.41-3.4c.38-.38.59-.89.59-1.42v-3.18c0-1.1-.9-2-2-2H8zm8 14.91v2.09c0 .55-.45 1-1 1H9c-.55 0-1-.45-1-1v-2.09c0-.27.11-.52.29-.71l3.71-3.7 3.71 3.71c.18.18.29.44.29.7z"
			></path>
		</svg>
	)
}

export default IconSession
