import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconLocation: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="none" viewBox="0 0 25 25">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				d="M13 22.412c2.01-2.514 6.592-8.608 6.592-12.03a7.521 7.521 0 00-7.52-7.52 7.521 7.521 0 00-7.519 7.52c0 3.422 4.582 9.516 6.592 12.03.481.6 1.374.6 1.856 0zm-.927-9.524a2.509 2.509 0 01-2.507-2.507 2.509 2.509 0 012.507-2.506 2.509 2.509 0 012.506 2.506 2.509 2.509 0 01-2.506 2.507z"
			></path>
		</svg>
	)
}

export default IconLocation
