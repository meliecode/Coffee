import React from 'react'

type PropTypes = {
	active?: boolean
}

const IconIceCream: React.FC<PropTypes> = ({ active }) => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="none" viewBox="0 0 25 25">
			<path
				fill={active ? '#bc8f98' : '#6D6875'}
				d="M18.979 8.334h-.04a6.182 6.182 0 10-12.285 0h-.04a2.06 2.06 0 000 4.122h12.365a2.061 2.061 0 000-4.122zm-7.411 14.327a1.354 1.354 0 002.457 0l4.267-8.831H7.3l4.267 8.83z"
			></path>
		</svg>
	)
}

export default IconIceCream
