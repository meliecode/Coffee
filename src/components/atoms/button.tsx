import React from 'react'
import styles from './button.module.scss'

interface PropTypes {
	children: React.ReactNode
	className?: string
}

const Button: React.FC<PropTypes & React.ButtonHTMLAttributes<any>> = ({ children, className, ...props }) => {
	return (
		<div className={`${styles.container} ${className}`} {...props}>
			{children}
		</div>
	)
}

export default Button
