import React from 'react'
import styles from './card.module.scss'

interface PropTypes {
	children: React.ReactNode
	active?: boolean
}

const Card: React.FC<PropTypes & React.ButtonHTMLAttributes<any>> = ({ children, active, ...props }) => {
	return (
		<div className={`${styles.container} ${active ? styles.active : ''}`} {...props}>
			{children}
		</div>
	)
}

export default Card
