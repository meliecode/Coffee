import React from 'react'
import styles from './dots.module.scss'

interface PropTypes {
	step: number
}

const Dots: React.FC<PropTypes & React.ButtonHTMLAttributes<any>> = ({ step }) => {
	return (
		<div className={styles.container}>
			<div className={`${step === 1 ? styles.active : ''} ${step >= 1 ? styles.actived : ''}`}></div>
			<div className={`${step === 2 ? styles.active : ''} ${step >= 2 ? styles.actived : ''}`}></div>
			<div className={`${step === 3 ? styles.active : ''} ${step >= 3 ? styles.actived : ''}`}></div>
			<div className={`${step === 4 ? styles.active : ''} ${step >= 4 ? styles.actived : ''}`}></div>
		</div>
	)
}

export default Dots
