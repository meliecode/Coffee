import React, { useContext } from 'react'
import Image from 'next/image'
import styles from './previous.module.scss'

import { UserContext } from '@context'

const Previous = ({ ...props }) => {
	const { activeStep, setActiveStep } = useContext(UserContext)

	return (
		<div className={styles.container} {...props}>
			<Image src="/images/previous.svg" width={13} height={13} alt="Previous" />
			Précédent
		</div>
	)
}

export default Previous
