import React from 'react'
import styles from './miniCard.module.scss'

interface PropTypes {
	children: React.ReactNode
	active?: boolean
}

const MiniCard: React.FC<PropTypes & React.ButtonHTMLAttributes<any>> = ({ children, active, ...props }) => {
	return (
		<div className={`${styles.container} ${active ? styles.active : ''}`} {...props}>
			{children}
		</div>
	)
}

export default MiniCard
