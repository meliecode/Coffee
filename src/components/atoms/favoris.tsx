import React from 'react'
import Image from 'next/image'
import styles from './favoris.module.scss'

const Favoris = () => {
	return (
		<div className={styles.fav}>
			<Image src="/images/fav.svg" width={10} height={10} alt="Favoris" />
			<span>Ajouter en favoris</span>
		</div>
	)
}

export default Favoris
